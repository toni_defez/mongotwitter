
const mongoose = require("mongoose");
const mensajeSchema = new mongoose.Schema({
    autor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    text:
    {
        type: String,
        required: true,
        trim:true,
        min: 5
    },
    fecha:
    {
        type:String,
        required:true,
        trim:true
    },
   hora:
   {
      type:String,
      required:true,
      trim:true
   }

});
const Mensaje = mongoose.model('mensaje', mensajeSchema);

module.exports = Mensaje;
