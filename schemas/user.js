const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        minlength: 1,
        trim: true,
        unique:true
    },
    password: {
        type: String,
        required: true,
        minlength: 4
    },
    consumerKey:{
        type:String,
        required:true,
        minlength: 1,
        trim: true
    },
    consumerSecret:{
        type:String,
        required:true,
        minlength: 1,
        trim: true
    },
    accesToken:{
        type:String,
        required:true,
        minlength: 1,
        trim: true
    },
    accesTokenSecret:{
        type:String,
        required:true,
        minlength:1,
        trim:true
    },
    playerId:{
        type:String,
        minlength:1
    }
   
});
const User = mongoose.model('user', userSchema);

module.exports = User;