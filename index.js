
const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");

const User1 = require("./schemas/user");
const Mensaje1 = require("./schemas/mensaje");

const fs = require('fs');
const path = require('path');
const sha256 = require("sha256");
const jwt = require('jsonwebtoken');

const tokenPass = 'qfkwejfewqqewfg';

//para el uso correo
const nodemailer = require('nodemailer');
const exphbs = require('express-handlebars');
const generator = require('generate-password');

//para el uso de OneSignal
var OneSignal = require('onesignal-node');
var moment = require('moment');

const app = express();

//para la publicacion de Tweets
var Twitter = require('twitter');


//view engine setup
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');


//static folder
app.use('/public', express.static(path.join(__dirname, 'public')));


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/twitterApp', { useMongoClient: true });




function EnviarNotificacion(user,text) {
    //para OneSignal
    var myClient = new OneSignal.Client({
        userAuthKey: 'NDRiYTgxMzUtYTEyYi00NTRkLWE1MzctMGU4MDkxNWE0NTk5',
        // note that "app" must have "appAuthKey" and "appId" keys
        app: {
            appAuthKey: 'NzI2YzE2OGQtZjIzMi00YjQ3LWFlY2QtZDI0MmY0NDEyMGEz',
            appId: 'd7bb668d-55a2-447d-b179-e3475cee1e09'
        }
    });
    var firstNotification = new OneSignal.Notification({
        contents: {
            en: "Tweet enviado:"+text,
            tr: "El tweet programado ha sido enviado!!�"
        }
    });

    //f614febb-8375-462e-bd8c-9d7d0e144e88
    //Mirar el usuario que nos interesa y dos veces su PLAYERID
    firstNotification.setTargetDevices([user.playerId, user.playerId]);
    if(text !=null )
    {
    myClient.sendNotification(firstNotification)
        .then(function (response) {
            console.log(response.data, response.httpResponse.statusCode);
	   console.log("Notificacion enviada");
        })
        .catch(function (err) {
            console.log('Something went wrong...', err);
        });
    }
}


//funcion para publicar automaticamente mensajes
async function myFunc() {

    function DeleteTweet(tweet)
    {
       console.log(tweet);
        Mensaje1.findByIdAndRemove(tweet._id).then(result => {
            console.log('Tweet borrado');
            return true;
        }).catch(error => {
           return false;
        });
    }

   



    Mensaje1.find().populate('autor').then(result => {
        result.map(function (doc) {
            let dia = doc.fecha;
            let horaMinuto = doc.hora;

            var fechaMensaje = moment(`${dia} ${horaMinuto}`, 'DD/MM/YYYY HH:mm').toDate();
            var fechaActual = new Date();

            if (fechaMensaje <= fechaActual) {
		console.log('Empieza el proceso...');
                PublicarTweet(doc.text,doc.autor);
                EnviarNotificacion(doc.autor,doc.text);
                DeleteTweet(doc);

            }
        })
    }).catch(error => {
        let data = {
            ok: false,
        };
        console.log(data);
    });


}

function PublicarTweet(texto, usuario)
{
    var client = new Twitter({
        consumer_key: usuario.consumerKey,
        consumer_secret: usuario.consumerSecret,
        access_token_key: usuario.accesToken,
        access_token_secret: usuario.accesTokenSecret
      });

    client.post('statuses/update', {status: `${texto}`}, function(error, tweet, response) {
        if (!error) {
          console.log(tweet);
        }
      });

    console.log("Tweet publicado");
}


//PublicarTweet();


//myFunc();
setInterval(myFunc, 1500);



function generateToken(user) {
    let token = jwt.sign({ _id: user._id, email: user.email }, tokenPass,
        { expiresIn: "2 hours" });
    return token;
}

function validateToken(token) {
    try {
        let user = jwt.verify(token, tokenPass);
        return user;
    } catch (e) {
        return null;
    }
}





//metodos de prueba 
app.get('/test2', (req, res) => {
    res.render('contact')
})

app.post('/recuperarPassword', (req, res) => {

    var password = generator.generate({
        length: 10,
        numbers: true
    });


    const output = `
      <p>Se ha generado una nueva contraseña para tu cuenta</p>
      <ul>  
        <li>Email: ${req.body.email}</li>
        <li>Password: ${password}</li>
      </ul>
    `;

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'rocproyect@gmail.com', // generated ethereal user
            pass: 'prueba123'  // generated ethereal password
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"RocProyect" <rocproyect@gmail.com>', // sender address
        to: req.body.email, // list of receivers
        subject: 'Recuperación contraseña', // Subject line
        text: 'Hello world?', // plain text body
        html: output // html body
    };


    User1.findOneAndUpdate({ "email": req.body.email }, {
        $set: { password: sha256(password) }
    }, { new: true }).then(result => {
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            res.render('contact', { msg: 'Email has been sent' });
        });

        let data = { ok: true };
        res.send(data);
    }).catch(error => {
        let data = {
            ok: false
        };
        res.send(data);
    });
});

app.get('/test', (req, res) => {
    res.send('Hola caracola');
})

//  METODOS PARA LOS SERVICIOS DE LOS CLIENTES

//registrar nuevo usuario
app.post('/register', (req, res) => {
    let newUser = new User1({
        email: req.body.email,
        password: sha256(req.body.password),
        consumerKey: req.body.consumerKey,
        consumerSecret: req.body.consumerSecret,
        accesToken: req.body.accesToken,
        accesTokenSecret: req.body.accesTokenSecret
    });
    newUser.save().then(result => {
        let data = { ok: true };
        res.send(data);
    }).catch(error => {
        let data = {
            ok: false,
            error: "User couldn't be registered"
        };
        res.send(data);
    });
});

//comprobar que un usuario existe
app.post('/login', (request, response) => {
    request.header["Content-type"] = "application/json"
    User1.findOne({
        email: request.body.email,
        password: sha256(request.body.password)
    }).then(user12 => {
        response.send(JSON.stringify({
            ok: true,
            token: generateToken(user12),
            userId: user12._id
        }));
        res.end();
    }).catch(error => {
        response.send(JSON.stringify({
            ok: false,
            error: 'User or password incorrect'
        }));
        res.end();
    });
});

//saca todos los usuarios registrados
app.get('/user', (request, response) => {

    User1.find().then(result => {
        response.send(JSON.stringify(result));
    })

});


//nos da las keys para poder conectarse a TweetInvi
app.post('/userDetails', (req, res) => {
    const tuser = validateToken(req.body.tokenPass);
    if (!tuser) {
        res.sendStatus(401);
        return;
    }
    else {
        User1.findOne({ "_id": req.body.id }).then(result => {
            res.send(JSON.stringify(result));
            res.end();
        })
    }

});

//borrar usuario -->Para darse de baja

app.post('/userDelete', (req, res) => {
    const tuser = validateToken(req.body.tokenPass);
    if (!tuser) {
        res.sendStatus(401);
        return;
    }
    else {
        User1.findByIdAndRemove(req.body.id).then(result => {
            let data = { ok: true };
            res.send(data);
        }).catch(error => {
            let data = {
                ok: false,
                errorMessage: "Error removing User " + req.params.id
            };
            res.send(data);
        });
    }
});



app.put('/pushInit', (req, res) => {

    const tuser = validateToken(req.body.tokenPass);
    if (!tuser) {
        res.sendStatus(401);
        return;
    }
    else {

        User1.findOneAndUpdate({ "_id": tuser._id }, {
            $set: {
                playerId: req.body.playerId
            }
        }, { new: true }).then(result => {
            let data = { ok: true };
            console.log(result);
            EnviarNotificacion(result);
            res.send(data);
        }).catch(error => {
            let data = {
                ok: false,
                errorMessage: "Error updating User:" + tuser._id
            };
            res.send(data);
        });


    }
})

//actualizar usuario -->Para poder cambiar contraseña
app.put('/user', (req, res) => {
    const tuser = validateToken(req.body.tokenPass);
    if (!tuser) {
        res.sendStatus(401);
        return;
    }
    else {

        User1.findOneAndUpdate({ "_id": tuser._id }, {
            $set: {
                email: req.body.email,
                password: sha256(req.body.password)
            }
        }, { new: true }).then(result => {
            let data = { ok: true, user: result };
            res.send(data);
        }).catch(error => {
            let data = {
                ok: false,
                errorMessage: "Error updating User:" + tuser._id
            };
            res.send(data);
        });
    }


});



//METODOS PARA LOS TWEETS

//subir tweet

app.post('/tweet', (req, res) => {
    const tuser = validateToken(req.body.tokenPass);
    if (!tuser) {
        res.sendStatus(401);
        return;
    }
    else {
        let Comment = new Mensaje1({
            text: req.body.text,
            fecha: req.body.fecha, hora: req.body.hora,
            autor: tuser._id
        });
        Comment.save().then(result => {
            let data = { ok: true, tweet: result };
            res.send(JSON.stringify(data));
        }).catch(error => {
            let data = {
                ok: false,
                errorMessage: "Error adding Tweet:"
            };
            res.send(data);
        });
    }
});


//Obtener todos los tweets de un usuario(Tuser ->autor)

app.post('/userTweet', (req, res) => {

    const tuser = validateToken(req.body.tokenPass);
    if (!tuser) {
        res.sendStatus(401);
        return;
    }
    else {
        Mensaje1.find({ "autor": tuser._id }).then(result => {
            let data = { ok: true, tweet: result };
            res.send(JSON.stringify(data));
        })
    }
});

//modificar tweet
app.put('/tweet', (req, res) => {
    const tuser = validateToken(req.body.tokenPass);
    if (!tuser) {
        res.sendStatus(401);
        return;
    }
    else {
        Mensaje1.findOneAndUpdate({ "_id": req.body.id }, {
            $set: {
                text: req.body.text,
                fecha: req.body.fecha,
                hora: req.body.hora
            }
        }, { new: true }).then(result => {
            let data = { ok: true, tweet: result };
            res.send(JSON.stringify(data));
        }).catch(error => {
            let data = {
                ok: false,
                errorMessage: "Error updating Tweet:" + req.body.id
            };
            res.send(data);
        });
    }

});

//borrar tweet
app.post('/tweetDelete', (req, res) => {
    const tuser = validateToken(req.body.tokenPass);
    if (!tuser) {
        res.sendStatus(401);
        return;
    }
    else {
        Mensaje1.findByIdAndRemove(req.body.id).then(result => {
            let data = { ok: true };
            res.send(data);
        }).catch(error => {
            let data = {
                ok: false,
                errorMessage: "Error removing Tweet " + req.params.id
            };
            res.send(data);
        });
    }
});







app.listen(5656, () => console.log('Server started')); 
